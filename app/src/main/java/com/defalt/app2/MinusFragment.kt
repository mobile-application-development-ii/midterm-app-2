package com.defalt.app2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.defalt.app2.databinding.FragmentMinusBinding

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class MinusFragment : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var _binding : FragmentMinusBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentMinusBinding.inflate(inflater, container, false)
        return _binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.btnimageEqual?.setOnClickListener {
            val num1 = binding?.inputNum1?.text.toString().toIntOrNull()
            val num2 = binding?.inputNum2?.text.toString().toIntOrNull()
            val result = num1?.minus(num2!!).toString()
            val action = MinusFragmentDirections.actionMiusFragmentToAnswerFragment(result)
            view.findNavController().navigate(action)
        }

    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            MinusFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}